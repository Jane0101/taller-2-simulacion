import simpy



def pedidos(env):
    while True:
        print("PEDIDO--BASICO--{0}".format(str(env.now)))
        d_basico = 10
        yield env.timeout(d_basico)

        print("PEDIDO--MEDIO--{0}".format(str(env.now)))
        d_medio = 25
        yield env.timeout(d_medio)

        print("PEDIDO--PREMIUN--{0}".format(str(env.now)))
        d_premiun = 55
        yield env.timeout(d_premiun)

if __name__== '__main__':
    env = simpy.Environment()
    env.process(pedidos(env))
    env.run(until=60)



