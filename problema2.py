import simpy



def prueba(env):
    while True:
        print("FASE--PRIMERA--{0}".format(str(env.now)))
        d_primera = 3
        yield env.timeout(d_primera)

        print("FASE--SEGUNDA--{0}".format(str(env.now)))
        d_segunda = 8
        yield env.timeout(d_segunda)

        print("FASE--TERCERA--{0}".format(str(env.now)))
        d_tercera = 14
        yield env.timeout(d_tercera)

if __name__== '__main__':
    env = simpy.Environment()
    env.process(prueba(env))
    env.run(until=900)
